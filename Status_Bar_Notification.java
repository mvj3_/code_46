// Status Bar Notification
int notificationID = 10;
NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// Create the notification
// 创建通知
Notification notification = new Notification(R.drawable.yourIconId, "Put your notification text here", System.currentTimeMillis());
// Create the notification's expanded message
// 创建通知的扩展信息
// When the user clicks on it, it opens your activity
// 当用户点击时，会执行的动作
Intent intent = new Intent(this, YourActivityName.class);
PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
notification.setLatestEventInfo(this, "Put your title here", "Put your text here", pendingIntent);
// Show notification
// 显示通知
notificationManager.notify(notificationID, notification);